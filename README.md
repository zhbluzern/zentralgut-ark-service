# Goobi ARK Minter and Services

This repsitory combines several scripts to create a fully maintained service to create (mint) and bind an Archvial Resource Key (ARK) in Goobi Workflow.

* Creates an ARK identifier based on a given NAAN and self-defined shoulders
* Checks the existence of an ARK to avoid duplicate identifiers locally (in a sqlite-database) and globally against [n2t](https://n2t.org)
* Binds an ARK in n2t (that means, the target URL ist directly known at N2T with additional metadata)
* Writes the ARK to metadata-file in Goobi Workflow and makes a journal entry if succeeded

## Setup

* Clone this repository and run the setup.py: `python3 setup.py`

1. The [ARKBeizli](https://gitlab.com/zhbluzern/arkbeizli) repository will be cloned in the `src` Directory
2. Several inputs ask you for invididual information add them carefully:
   a. The setup creates the sqlite-database in `database/`
   [!Information]
   If you run the setup later on, and a sqlitedatabase already exists, you can enter the name of this database. the setup procedure will have effect on it, insert of the table is only done, if not exists.
   b. The setup creates the two configuration files `config.json` and `.env`

```bash
Please enter the filename for the SQLite database (default: arkDb.sqlite): arkDB_Test
Enter your NAAN [default: 99999]: 
Enter your default Shoulder [for test naan use fk4]: 
Enter your default Ark-Structure [default: seddek]
Enter your n2t-User (optional): 
Enter your n2t-Password (optional): 
Enter your n2t-URL (default: https://n2t.net):
Enter the absolute path to your Goobi Metadata Directory: 
Enter your Goobi Workflow REST-API Token: 
Enter the REST-API-URL of your Goobi Workflow:
************************************
Database 'database/arkDB_Test.sqlite' has been successfully created with table 't_ark'.
************************************
Use ARKPyCli now, go to the your favourite Ark-Beizli grap a coffee and type: python3 main.py --help
```

### Configurations

The Goobi Workflow ARK Service uses two configuration files: `.env` and `config.json`. The `.env` file contains secrets and general file based information, the `config.json` stores more Goobi specifi configuration, e.g. the mapping of `DocStrctTypes` to the n2t `how`-Value or the mappings of Goobi Projects to ARK-Shoulders.

Both files are created during the `setup.py` process. Please add invidiual goobi information afterwards directly in the config.json (e.g. a full list of your Goobi Projects mapped to a should if necessary)

### General credentials in .env

```
#n2t-configuration
username=YOUR_N2T_USER
password=N2T_User_Password
n2turl=N2T_BASE_URL

#sqlite-configuration
database=FILENAME

#ark-configuration
naan=YOUR-NAAN
arkSchema=seddek

#goobi-configuration
metadataDir=GOOBI-METADATA-DIR
apiToken=GOOBI-WORKFLOW-API-TOKEN for /process/journals and PUT/DELETE/POST
goobiApiUrl=URL_TO_YOUR_WORKFLOW_REST_API/
```

### Goobi related configurations, e.g. metadata mapping

```json

{
    "metadataDir" : "PATH_TO_GOOBI_METADATA",
    "shoulders":
    [
        /* List of Shoulders - key maps to Goobi Project*/
        { "Goobi_Project" : "SHOULDER"}
    ],
    "defaultShoulder" :  "YOUR_DEFAULT_SHOULDER",
    "naan" : "YOUR_NAAN",
    "arkSchema" : "seddek",
    "targetUrl" : "YOUR_GOOBI_TARGET_URL",
    "howDocStrctMapping" : [
        /* List of DocStructTypes mapped to n2t-how-Types */
        {"Graphic" : "image"},
        {"Monograph" : "text"},
        {"Periodical" : "text"}
    ]
}
```

### Filedatabase (sqlite)

The filedatabase will be created during the `setup.py` process in the `database/` directory.

The database contains one table `t_ark` with two columns (text value) `ark` and `target`

```bash
$ sqlite3 {databasename}.sqlite
sqlite>    create table t_ark(ark text, target text);
```

## Usage

### Use `main.py`

* `python3 main.py --help` shows you different options of the main procedure

#### Mint an ARK

The command `python3 main.py -m` creates a new ark and stores it in the local database.

If you add a `--target {TARGET_URL}` : `python3 main.py -m -t https://YOUR.TARGET.URL` - both the new ARK and target are stored in the database

#### Mint and bind an ARK

If you have credentials for n2t.net you can bind a new created ARK to n2t:

`python3 main.py -mb -t https://YOUR.TARGET.URL`

#### Export your local filedatabase to a csv

`python3 main.py -o allMyArksOutputFileName.csv`

### Use `run_fromGoobi.py` as ARK minting service in Goobi Workflow

* Register a [NAAN](https://arks.org/about/ark-naans-and-systems/) for your repository or organization: [NAAN-Request-Form](https://docs.google.com/forms/d/e/1FAIpQLSfd1CX6idwLB47g8OGKUG654auV8IU8yI7DAs61cXGOoFDn0g/viewform?c=0&w=1)
  * Add the default Viewer-ARK-Resolver-URL as base target: `https://YOUR_VIEWER.ORG/resolver?field=MD_PI_ARK&identifier=`
* Prepare in Workflow in the `ruleset.xml` a metadata field `ARK`

```xml
<Preferences>
  <MetadataType>
    <Name>ARK</Name>
    <language name="de">ARK</language>
    <language name="en">ARK</language>
  </MetadataType>
[...]
</Preferences>

<Formats>
 <METS>
   <Metadata>
        <InternalName>ARK</InternalName>
        <WriteXPath>./mods:mods/#mods:identifier[@type='ark']</WriteXPath>
    </Metadata>
 </METS>
 [...]
</Formats>
```

* Prepare the mapping of ARK in the `config_indexer.xml`

```xml
<fields>
<MD_PI_ARK>
    <list>
        <item>
            <xpath>mets:xmlData/mods:mods/mods:identifier[@type="ark"]</xpath>
            <getnode>first</getnode>
            <addToDefault>true</addToDefault>
        </item>
    </list>
</MD_PI_ARK>
</fields>
```

* For total service in Goobi Workflow ("mint and bind an ARK") call the script the following way:

  * `python3 run_fromGoobi.py -m -id {PROCESS_ID} -p {PROJECT_NAME} --script_path {ABSOLUTE_PATH_TO_SCRIPT_DIRECTORY}`
  * Add the flag parameter `-m (--mint)` or `-b (--bind)`. Use `bind` only if you have a n2t.net user to write directly metadata and the target url there, otherweise `-m` will be enough.
  * You can call the script as Goobi Workflow Step with the Script Parameter. The necessary arguments ProcessID and Project-Name are available variables in Goobi, add additionally also the absolute path to the Script-Folder so that Tomcat is able to use all dependencies and to have access to the sqlite database.
* To have a success information in the Process Journal, create an API Token in Goobi Workflows `goobi_rest.xml` with access to the PUT Method of `/process/journal` REST-API-endpoint and add the information to the `.env` file

## Credits

`pymint.py` is forked from https://github.com/vt-digital-libraries-platform/NOID-mint (under MIT-License)

All other scripts are developed by [ZHB Luzern](https://zhbluzern.ch) and published under the [MIT-0 No Attribution License](LICENSE)
