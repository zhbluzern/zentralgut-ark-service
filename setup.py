import os
import sqlite3
import subprocess
import json

def clone_or_update_git_repo(repo_url, dest_folder):
    # Get the absolute path of the directory where this script resides (root directory)
    root_dir = os.path.abspath(os.path.dirname(__file__))

    # Combine with the desired destination folder to build an absolute destination path
    dest_folder = os.path.join(root_dir, dest_folder)

    # Check if the destination folder exists
    if os.path.exists(dest_folder) and os.path.isdir(dest_folder):
        try:

            # Change directory to the existing repository
            #os.chdir(dest_folder)
            print(f"local path: {dest_folder}")
            os.chdir(dest_folder)
            # Pull the latest changes
            subprocess.run(["git", "pull", "origin", "master"], check=True)
            print(f"Updated repository in {dest_folder}")

        except subprocess.CalledProcessError as e:
            print(f"Failed to update the repository. Error: {e}")
            try:
                # Clone the repository if it doesn't exist
                subprocess.run(["git", "clone", repo_url, dest_folder], check=True)
                print(f"Cloned repository into existing but empty {dest_folder}")
                os.chdir(root_dir)
            except subprocess.CalledProcessError as e:
                print(f"Failed to clone the repository in the empty directory. Error: {e}")
            #os.chdir(os.path.dirname(os.getcwd()))
            os.chdir(root_dir)

        finally:
            #Change back to the original directory"
            os.chdir(root_dir)

    else:
        try:
            # Clone the repository if it doesn't exist
            subprocess.run(["git", "clone", repo_url, dest_folder], check=True)
            print(f"Cloned repository into {dest_folder}")

        except subprocess.CalledProcessError as e:
            print(f"Failed to clone the repository. Error: {e}")

def main():
    root_dir = os.getcwd()
    print(f"root dir: {root_dir}")
    # Example: Clone a Git repoository into the src directory
    repo_url = 'https://gitlab.com/zhbluzern/arkbeizli.git'  # Replace with the actual repository URL
    dest_folder = 'src/arkbeizli'  # Destination folder path
    clone_or_update_git_repo(repo_url, dest_folder)

    # Set default database name
    default_db_name = "arkDb.sqlite"
    print(f"local path: {os.getcwd()}")
    # Prompt for the database filename with default name
    db_name = input(f"Please enter the filename for the SQLite database (default: {default_db_name}): ") or default_db_name
    # Check if the filename has an extension, append '.sqlite' if not
    if '.' not in db_name:
        db_name += ".sqlite"

    # Additional .env variables
    naan = input("Enter your NAAN [default: 99999]: ") or "99999"
    shoulder = input("Enter your default Shoulder [for test naan use fk4]: ") or ""
    noidTemplate = input("Enter your default Ark-Structure [default: seddek]") or "seddek"
    n2t_user = input("Enter your n2t-User (optional): ") or ""
    n2t_password = input("Enter your n2t-Password (optional): ") or ""
    n2t_url = input("Enter your n2t-URL (default: https://n2t.net): ") or "https://n2t.net"
    targetUrl = input("Enter your Goobi-Viewer-Image-Path as base target Url (like https://viewer.goobi.io/image/): ") or ""
    metadataDir = input("Enter the absolute path to your Goobi Metadata Directory: ") or ""
    apiToken = input("Enter your Goobi Workflow REST-API Token: ") or ""
    goobiApiUrl = input("Enter the REST-API-URL of your Goobi Workflow: ")  or ""

    # Create path for the database directory
    db_dir = os.path.join(os.getcwd(), 'database')
    if not os.path.exists(db_dir):
        os.makedirs(db_dir)

    # Create database file path
    db_path = f"{db_dir}/{db_name}"

    # Create database and table
    conn = sqlite3.connect(db_path)
    cursor = conn.cursor()
    cursor.execute("CREATE TABLE IF NOT EXISTS t_ark(ark TEXT, target TEXT);")
    conn.commit()
    conn.close()

    # Save path and other variables in the .env file
    env_path = os.path.join(os.getcwd(), '.env')
    with open(env_path, 'w') as env_file:
        env_file.write("#sqlite-configuration\n")
        env_file.write(f"database=database/{db_name}\n")
        env_file.write("\n#ark-configuration\n")
        env_file.write(f"naan={naan}\n")
        env_file.write(f"shoulder={shoulder}\n")
        env_file.write(f"noidTemplate={noidTemplate}\n")
        env_file.write("\n#n2t-configuration\n")
        if n2t_user:
            env_file.write(f"username={n2t_user}\n")
        if n2t_password:
            env_file.write(f"password={n2t_password}\n")
        if n2t_url:
            env_file.write(f"n2turl={n2t_url}\n")
        env_file.write("\n#goobi-configuration\n")
        if n2t_user:
            env_file.write(f"metadataDir={metadataDir}\n")
        if n2t_password:
            env_file.write(f"apiToken={apiToken}\n")
        if n2t_url:
            env_file.write(f"goobiApiUrl={goobiApiUrl}\n")  

    # Create config.json with given information
    goobiConfig = {}
    goobiConfig["metadataDir"] = metadataDir
    goobiConfig["shoulders"] = [{"test":"fk4"}]
    goobiConfig["defaultShoulder"] = shoulder
    goobiConfig["naan"] = naan
    goobiConfig["noidTemplate"] = noidTemplate
    goobiConfig["targetUrl"] = targetUrl
    goobiConfig["howDocStrctMapping"] = [
        {"Graphic" : "image"},
        {"Monograph" : "text"},
        {"Periodical" : "text"},
        {"Newspaper" : "text"},
		{"Photograph" : "image"},
		{"Video" : "video"},
		{"Incunable" : "text"},
		{"Manuscript" : "text"},
		{"Act" : "text"},
		{"SingleMap" : "image"},
		{"Folder" : "oba"},
		{"Postcard" : "image"},
		{"Object" : "image"}
    ]
    configJsonPath = os.path.join(os.getcwd(), 'config.json')
    with open(configJsonPath, 'w') as configJson:
        json.dump(goobiConfig, configJson, indent=4)

    # Print Success Information of Setup Process
    print(f"************************************")
    print(f"Database 'database/{db_name}' has been successfully created with table 't_ark'.")
    print(f"Your given configuration variables are saved in '.env'.")
    print(f"************************************")
    print(f"Use ARKPyCli now, go to the your favourite Ark-Beizli grap a coffee and type: python3 main.py --help")

if __name__ == "__main__":
    main()

