﻿import src.arkbeizli.src.sqlark.sqlark as sqlark
import src.arkbeizli.src.n2t.n2tbind as n2t
import src.run_mint as mint
import src.goobiHandler.goobiHandler as goobi
import src.goobiHandler.goobiJournal as goobiJournal
import src.goobiHandler.goobi4ARK as goobiARK
import argparse
import os
import json

# Erstelle den ArgumentParser
parser = argparse.ArgumentParser(description='Mint an ARK for a Goobi Workflow Process', usage="python3 run.py --vorgangId --project")
# Füge das Argument 'shoulder' hinzu
parser.add_argument('-m', '--mint', action='store_true', help='With flag -m you mint a new Ark (with duplicate check in the local filebase and against n2t.net)')
parser.add_argument('-b', '--bind', action='store_true', help='With flag -b your ark will be bound against n2t.net')
parser.add_argument('--vorgangId', '-id', required=True, help='Goobi-Vorgang-ID, dient zum Auslesen und Schreiben der Metadatan', )

parser.add_argument('--project', '-p', required=False, help='Goobi-Project um Shoulder zu identifizieren', )
parser.add_argument('--docStructType', '-t', required=False, help='Übermittelt den Dokumenttype für das how-Value Mapping')
parser.add_argument('--script_path', '-script', required=False, help='Wenn Aufruf über Goobi-Vorgang-Step erfolgt, hier vollen Pfad zum Verzeichnisses des Skripts übermitteln')
# Parse die Argumente
args = parser.parse_args()

if not args.mint and not args.bind:
    parser.error("No Run Parameter (--mint OR --bind) given")

if args.mint and args.bind and not args.vorgangId:
    parser.error("To bind an ARK against n2t a Goobi Workflow Process Id is needed!")

# Zugriff auf das Argument 'vorgangId'
print(f"Empfangene Vorgang-ID: {args.vorgangId}")
if args.vorgangId:
    vorgangId = args.vorgangId
else:
    vorgangId = ""
# Zugriff auf das Argument 'target'
print(f"Empfangenes Project: {args.project}")
if args.project:
    project = args.project
else:
    project = ""
#Zugriff auf das Argument 'script_path'
if args.script_path:
    os.chdir(args.script_path)
#Zugriff auf das Argument docStructType
if args.docStructType:
    docType = args.docStructType
else:
    docType = None

#Zugriff auf config.json
with open('config.json', 'r') as myfile:
    data=myfile.read()
config = json.loads(data)

# Query Shoulder based on Project-Title and config.json Mapping
shoulder = goobiARK.getShoulder(config, project)

#Initialize ArkDB-Class
arkDb = sqlark.sqlark(databasePath=os.getenv("database"))

#Initialize and read meta.xml
metadataPath = config["metadataDir"]+f"/{vorgangId}/meta.xml"
modsXml = goobi.parseMetadata(metadataPath)
#Check whether ARK is already set in meta.xml
setArk = goobi.findMetadata(modsXml, "ARK")
try:
    print(f"ARK already set: {setArk[0]} - go forward to update metadata and targetUrl in n2t")
    newArk = None
    bindArk = setArk[0]
    #Check whether ARK also in local filedb and bound in n2t?
except:
    print("mint a new ARK")
    newArk = mint.mintArk(arkDb,config["naan"],shoulder,config["noidTemplate"])

# Fetch Metadata from meta.xml File
## Get CatalogIDDigital for Target-URL
catalogIdDigital = goobi.findMetadata(modsXml, "CatalogIDDigital")
targetUrl = config["targetUrl"]+catalogIdDigital[0]
print(f"_t: {targetUrl}")


## find DocStructType and map it to n2t-howType
howValue = goobiARK.mapDocStruct2How(modsXml, config, docType)
print(f"how: {howValue}")

#Title
title = goobi.findMetadata(modsXml, "TitleDocMain")
print(f"what: {title[0]}")

#PublicationYear
when = goobiARK.getPubYear(modsXml)

#Related Persons
whoString = goobiARK.getRelatedPersons(modsXml)

# store ARK and target in Database
if args.mint:
    if newArk:
        bindArk = newArk  
        arkDb.storeArk(newArk, targetUrl)
        #Write ARK to Goobi Meta.xml
        goobi.backupMetaFile(vorgangId, modsXml, config["metadataDir"] )
        goobi.addMetadata(modsXml, value=bindArk, attributeName="ARK")
        goobi.writeNewMetaFile(vorgangId, modsXml, config["metadataDir"])
        goobiJournalMessage = f"{bindArk} angelegt"
    elif setArk:
        bindArk = setArk[0]
        arkDb.updateArk(bindArk, targetUrl)
        goobiJournalMessage = f"{bindArk} aktualisiert"

    #Write Info in Goobi Journal
    #Variable goobiJournalMessage beinhaltet messageString
    print(goobiJournalMessage)
    goobiJournal.writeJournal(vorgangId, os.getenv("apiToken"), journalMessage=goobiJournalMessage, journalType="info")

# bind ARK to n2t
if args.bind:
    n2t.bindArkWithMd(ark=bindArk, target=targetUrl, who=whoString, title = title[0], when=when, docType=howValue)
    print(f"ARK https://n2t.net/{bindArk}?? bound in n2t: {n2t.existsArk(bindArk)}")