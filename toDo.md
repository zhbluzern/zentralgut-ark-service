# ToDo

## Minter

* [X] Strukturierte(re) Behandlung der Shoulder - aktuell werden sie im Template übergeben (`template=f"{shoulder}.seededk`) im Prinzip eh nicht schlehct
* [X] Check ob gemintede ARK bereits lokal existiert (via DB-Call)
* [X] Check ob gemintede ARK bereits in n2t existiert (n2tbind.py hat Methode `existsArk(ark):` und retourniert `True` or `False`)
* [X] there is a problem with `if newArk:` when ark exists and no newArk is minted at the beginning
* [X] Combine mint, parse metadata and bind in `run.py`
* [X] Test with https://zentralgut.ch/image/9914334435205505/1/LOG_0000/, https://zentralgut.ch/image/9914438487305505/1/LOG_0000/ - mint and bind ark, write to Goobi
* [X] Test with periodical Volume (fo fetch the correct issue CatalogIDDigital)
* [X] Test with meta.xml from other project
* [X] ARK-Schema-Template configurable in .env File

## FileDatabase

* [X] Setup SQLITE-DB mit ARK, ~~Shoulder~~ (ist eh Teil der ARK), targetUrl, ~~Metadata-JSON~~ (besser live)
* [X] Neue Prod-Datenbank erstellen, Import aller ARKs.

## N2T

* [X] Call N2T to bind ARK
* [X] The `bindArk()` Method should only bind the target to an ark, metadata should be added with the separate `setArkMd()`
* [X] Change the current `bindArk()` method to a `bindArkWithMd()` method, to bind a new ark with necessary metadata (only the `erc` ones) in one api-call for faster bulk operations
* [X] test the `:hx` command for special characters according to the n2t-api-doc.
  :Ein fingierter Testitel `"D'Musikante chömid:" - Zur Tradition der [Obwaldner] (Bläsertanzmusik)=` mit `",',(),[],:,=` und Umlauten hat mit einzig dem zusätzlichen Befehl des Escaping der " fehlerfrei nach n2t funktioniert. Im Gegensatz dazu hat das `:hx` command hat am Ende der Qutotation-marks im Titel den Rest des Titels abgeschnitten.
* [X] Way to perform script without n2t access - do it with -m and -b parameter like in Arkbeizli

## Goobi `run_fromGoobi.py`

* [X] Urheber -> nur die Creator aus dem ersten `<mods>` Block holen.
* [X] Call for Metadata `CatalogIDDigital`, `TitleDocMain`, `Creator`, `DocStrctType`, `PublicationYear`
* [X] Write ARK to SQLITE
* [X] Bind ARK to N2T
* [X] Write ARK to Goobi
* [X] Check whether ARK is set in `meta.xml` true: update ARK / false: mint ARK
* [X] Deploy on Workflow and test the script (~~virtualenv necessary~~)
* [X] Write Info to Goobi Workflow Journal

## Misc

* [X] Ask https://github.com/vt-digital-libraries-platform/NOID-mint for License/Credits by Reuse - laut pypi (https://pypi.org/project/noid-mint/) MIT-License (Issue: https://github.com/vt-digital-libraries-platform/NOID-mint/issues/4)
* [X] Create config.json in `setup.py` with the given parameter and make empty keys for addtional goobi related infos.
