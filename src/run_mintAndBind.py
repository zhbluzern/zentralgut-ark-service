import pymint.pymint as ark
import sqlark.sqlark
import argparse
import n2t.n2tbind as n2t

# Erstelle den ArgumentParser
parser = argparse.ArgumentParser(description='Mint an ARK',epilog="What and where?", usage="python3 run.py --target={TARGET_URL} --naan={YOUR_NAAN} --shoulder={YOUR_SHOULDER}")
# Füge das Argument 'shoulder' hinzu
parser.add_argument('--target', '-t', required=True, help='Target URL auf die ARK zeigen soll', )
parser.add_argument('--naan', '-n', required=True, help='NAAN als erster fixer Teil einer ARK', )
parser.add_argument('--shoulder', '-s', required=False, help='Shoulder als zweiter, fixer Teil einer ARK', )
parser.add_argument('--schema', '-m', required=False, help='Schema of ARK-Identifier (how many digits/characters should the ark contain)', )
# Parse die Argumente
args = parser.parse_args()
# Zugriff auf das Argument 'shoulder'
print(f"Empfangene Shoulder: {args.shoulder}")
if args.shoulder:
    shoulder = args.shoulder
else:
    shoulder = ""
# Zugriff auf das Argument 'target'
print(f"Empfangene Shoulder: {args.target}")
if args.target:
    target = args.target
else:
    target = ""
# Zugriff auf das Argument 'NAAN'    
if args.naan:
    naan = args.naan
else:
    naan = "99999" #Test-NAAN
#Zugriff auf das Argument "Schema"
if args.schema:
    arkSchema = args.schema
else:
    arkSchema = "seddek"

#Initialize ArkDB-Class
arkDb = sqlark.sqlark.sqlark()
alreadyUsedArk = 1

# Variables for test purpose
#newArk = "ark:/63274/bz1sx4j" #that's an ark already stored in the test database - to test, that no duplicate arks are created!
""" newArk = ark.mint(template=f'{shoulder}.seddek', n=None, scheme='ark:/', naa=naan)
newArk = "ark:/63274/bz1f708f"
alreadyUsedArk = arkDb.arkInDb(newArk)
print (alreadyUsedArk) """

arkLoopCounter = 0
#Loop through new ark until a new Ark (alreadyUsedArk==None) is found fo given naan/shoulder
while alreadyUsedArk != None:
    #create a new ark like ark:/63274/bz1s46rh
    newArk = ark.mint(template=f'{shoulder}.{arkSchema}', n=None, scheme='ark:/', naa=naan)
    #to avoid duplicate arks check if "new" ark is already in use:
    alreadyUsedArk = arkDb.arkInDb(newArk) #check if minted ARK exists in local filebase
    #print(f"already used ark: {alreadyUsedArk}")
    if n2t.existsArk(newArk) == True: #check if newly minted ARK exists in n2t
        alreadyUsedArk = 1
    #print(f"already used ark: {alreadyUsedArk}")
    arkLoopCounter += 1

print(f"{str(arkLoopCounter)} try(s): {newArk}")
#store ARK and target in Database
arkDb.storeArk(newArk, target)

# bind ARK to n2t
n2t.bindArk(newArk, target)
print(f"ARK https://n2t.net/{newArk}?? bound in n2t: {n2t.existsArk(newArk)}")
#fetch Metadata from Goobi and set them to n2t

