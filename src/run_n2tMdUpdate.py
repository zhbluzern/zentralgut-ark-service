import argparse
import n2t.n2tbind as n2t

# Erstelle den ArgumentParser
parser = argparse.ArgumentParser(description='Bind Metadata to an ARK',epilog="What and where?", usage="python3 run_n2tMdUpdate.py --ark --mdType --mdValue")
# Füge das Argument 'shoulder' hinzu
parser.add_argument('--ark', '-a', required=True, help='ARK die aktualisiert werden soll', )
parser.add_argument('--mdType', '-m', required=True, help='Metadatum das aktulaisiert werden soll', )
parser.add_argument('--mdValue', '-v', required=True, help='Wert des zu aktualisierenden Metadatums', )
# Parse die Argumente
args = parser.parse_args()

print(f"Empfangene ARK: {args.ark}")
if args.ark:
    ark = args.ark
else:
    ark = ""

print(f"Metdataum to set: {args.mdType}")
if args.mdType:
    mdType = args.mdType
else:
    mdType = ""    

print(f"Value of Metadata: {args.mdValue}")
if args.mdValue:
    mdValue = args.mdValue
else:
    mdValue = ""     

n2t.setArkMd(ark, mdType, mdValue)