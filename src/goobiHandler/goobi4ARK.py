import src.goobiHandler.goobiHandler as goobi

# This Script contains different methods for more complex metadata wrangling, to make to run*.py easier to read

# Query Shoulder based on Project-Title and config.json Mapping
def getShoulder(config, project):
    shoulder = None
    for shoulders in config["shoulders"]:
        if project in shoulders:
            shoulder =  (shoulders[project])
            
    if shoulder is None:
        shoulder = (config["defaultShoulder"])
        print(f"fallback to default shoulder {shoulder} - project slug {project} not mapped in config.json or wrong typed in command call")
    else:
        print(f"shoulder for project {project}: {shoulder}")
    
    return shoulder

#Get the n2t-how: Value based on DocStrctType
def mapDocStruct2How(modsXml, config, docType=None):
    ## find DocStructType and map it to n2t-howType
    #<mets:div DMDID="DMDLOG_0000" ID="LOG_0000" TYPE="Monograph">
    if not docType:
        docStructType = goobi.findMetadata(modsXml,xpathStr="(//mets:structMap[@TYPE='LOGICAL'])[1]/mets:div[1]/@TYPE") 
        docType = docStructType[0]
    howValue = ""
    for docStructMapping in config["howDocStrctMapping"]:
        for key,value in docStructMapping.items():
            if key == docType:
                howValue = value
    if howValue == "":
        howValue = "(:unav)"
    return howValue

#Related Persons
def getRelatedPersons(modsXml):
    whoString = goobi.getPersonsAsString(modsXml)
    if whoString != "":
        print(f"who: {whoString}")
    else:
        whoString = "(:unav)"
        print(f"who: (:unav)")
    return whoString

#PublicationYear
def getPubYear(modsXml):
    pubYear = goobi.findMetadata(modsXml, "PublicationYear")
    try:
        when = pubYear[0]
        print(f"when: {pubYear[0]}")
    except:
        when = "(:unav)"
        print(f"when: (:unav)")
    return when