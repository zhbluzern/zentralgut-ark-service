import lxml.etree as ET
from datetime import datetime

def parseMetadata(filename):
    #mytree = ET.parse(f'metadata/{processId}/meta.xml',parser=ET.XMLParser(remove_blank_text=True))
    mytree = ET.parse(filename, parser=ET.XMLParser(remove_blank_text=True))
    mytree = mytree.getroot()
    return mytree

def findMetadata(mytree, attributeName="", xpathStr = ""):
    namespaces = {'mods' : 'http://www.loc.gov/mods/v3','mets' : 'http://www.loc.gov/METS/',  'goobi' :  'http://meta.goobi.org/v1.5.1/' }
    ET.register_namespace=namespaces
    if xpathStr == "":
        xpathStr = f".//goobi:metadata[@name='{attributeName}']/text()"
    #print(xpathStr)
    e = mytree.xpath(xpathStr, namespaces=namespaces)
    #print(e)
    return e

def getPersonsAsString(mytree):
    namespaces = {'mods' : 'http://www.loc.gov/mods/v3','mets' : 'http://www.loc.gov/METS/',  'goobi' :  'http://meta.goobi.org/v1.5.1/' }
    persons = findMetadata(mytree, xpathStr="(//goobi:goobi)[1]/goobi:metadata[@type='person']/goobi:displayName | (//goobi:goobi)[1]/goobi:metadata[@type='person']/@name")
    whoString = ""
    whoStringCounter = 1
    whoStringDet = ""
    for person in persons:
        try:
            whoStringDet = (person.text)+ whoStringDet
        except:
            whoStringDet = f" ({person})"

        if whoStringCounter % 2 == 0:  
            if whoString != "":
                whoString = whoString + "; "
            whoString = whoString + whoStringDet
            whoStringDet = ""
        whoStringCounter += 1
    return whoString

    
def addMetadata(mytree, value, attributeName):
    namespaces = {'mods' : 'http://www.loc.gov/mods/v3','mets' : 'http://www.loc.gov/METS/',  'goobi' :  'http://meta.goobi.org/v1.5.1/' }
    ET.register_namespace=namespaces
    xpathFindStr='.//mods:extension/goobi:goobi'
    e = mytree.find(xpathFindStr, namespaces)
    metadata='{http://meta.goobi.org/v1.5.1/}metadata'
    newMetadata = ET.SubElement(e, metadata)

    newMetadata.text = value
    newMetadata.set("name", attributeName)

    print(f"new insert: add {value} for MODS {attributeName}")
    return mytree

def writeNewMetaFile(processId, mytree, filePath):
    mytree = ET.ElementTree(mytree)
    mytree.write(f'{filePath}{processId}/meta.xml',encoding="UTF-8", xml_declaration=True, pretty_print=True)

def backupMetaFile(processId, mytree, filePath):
    mytree = ET.ElementTree(mytree)
    currentDateStr = datetime.utcnow().strftime('%Y-%m-%d-%H%M%S%f')[:-3]
    mytree.write(f'{filePath}{processId}/meta.xml.{currentDateStr}', encoding="UTF-8", xml_declaration=True, pretty_print=True)